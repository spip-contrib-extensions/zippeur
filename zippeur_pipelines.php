<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}
function zippeur_declarer_tables_principales($table) {
	$table['spip_zippeur'] = [
		'field' => [
			'id_zip'		=> 'INT',
			'nom'			=> 'text',
			'extension' => 'TINYTEXT',
			'date_modif'	=> 'datetime',
			'date_zip'		=> 'datetime',
			'delai_suppression' => 'INT',
			'fichiers' => 'INT'
		],

		'key' => ['PRIMARY KEY' => 'id_zip']

	];
	return $table;
}

function zippeur_taches_generales_cron($taches) {
	if (!defined('_ZIPPEUR_EFFACER_ZIP')) {
		define('_ZIPPEUR_EFFACER_ZIP', 2 * 3600);
	}
	$taches['zippeur_effacer_zip'] = _ZIPPEUR_EFFACER_ZIP;
	return $taches;
}
function zippeur_pre_liens($txt) {
	$match = [];
	$regexp = '#\[(.*)->(zip_doc_article|zip_doc_album)(\d*)\]#U';
	preg_match_all($regexp, $txt, $match, PREG_SET_ORDER);
	foreach ($match as $lien) {
		// construires les ≠ paramètres
		$objet      = str_replace('zip_doc_', '', $lien[2]);
		$id_objet   = $lien[3];
		$texte      = $lien[1] != '' ? $lien[1] : generer_info_entite($id_objet, $objet, 'titre', true) . ' - ' . _T('zippeur:ensemble_fichier');
		$nom_zip    = $objet . '_' . $id_objet;

		// constuire la liste des fichiers
		$fichiers   = [];
		$sql        = sql_select('maj,fichier', 'spip_documents INNER JOIN spip_documents_liens as L1', "spip_documents.statut='publie' AND L1.id_objet='$id_objet' AND L1.objet='$objet'", '', 'spip_documents.maj DESC');
		$first = true;
		$maj = '';
		while ($r = sql_fetch($sql)) {
			if ($first == true) {
				$maj = $r['maj'];
				$first = false;
			}
				$fichiers[] = copie_locale(get_spip_doc($r['fichier']));
		}
		// construire le zip
		$url_zip    = zippeur($fichiers, $maj, 'defaut', $nom_zip);

		// constuitre le lien
		$replace    = "<a href='$url_zip' type='application/zip' class='spip_in zippeur' title='$texte (" . taille_en_octets(filesize($url_zip)) . ")'>$texte</a>";
		$txt      = str_replace($lien[0], $replace, $txt);
	}
	return $txt;
}
