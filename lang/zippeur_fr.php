<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/zippeur.git

return [

	// E
	'ensemble_fichier' => 'Zip de l’ensemble des fichiers',
];
