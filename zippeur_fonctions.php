<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function zippeur_chemin_dossier_local() {
	static $chemin = '';
	if (!$chemin) {
		if (defined('_DIR_SITE')) {
			$chemin = _DIR_SITE . _NOM_TEMPORAIRES_ACCESSIBLES;
		}
		else {
			$chemin = _DIR_RACINE . _NOM_TEMPORAIRES_ACCESSIBLES;
		}
	}
	return $chemin;
}

include_spip('inc/zippeur_dynamique');
function zippeur_dynamique($dossier, $date, $cmd = 'defaut', $dynamiques = [], $statiques = [], $sanspath = [], $delai = 0, $extension = 'zip') {
	if ($date == '') {
		$date = date('Y-m-d H:i:s', time());
	}
	$chemin = zippeur_chemin_dossier_local() . $dossier;
	function_exists('supprimer_repertoire') ?  supprimer_repertoire($chemin) : spip_log('Version de SPIP < 3, possibilité de mélange dans un repertoire dynamique', 'zippeur');
	sous_repertoire($chemin);

	// création des fichiers dynamiques
	if (is_array($dynamiques)) {
		foreach ($dynamiques as $dyn) {
			if ($dyn[1] == '') { 	// si le 2 argument est vide, alors pas de souci, on prend le chemin tel quel
				$dyn[1] = $dyn[0];
			}
			zippeur_creer_fichier($dyn[0], $dossier . '/' . $dyn[1], $dyn[2]);
		}
	}
	// Les fichiers statiques
	if (is_array($statiques)) {
		foreach ($statiques as $stat) {
			if ($stat[1] == '') {		// si le 2 argument est vide, alors pas de souci, on prend le chemin tel quel
				$stat[1] = $stat[0];
			}

			if (is_dir(find_in_path($stat[0]))) {
				zippeur_copier_dossier($stat[0], $dossier . '/' . $stat[1]);
			} else { zippeur_copier_fichier($stat[0], $dossier . '/' . $stat[1]);
			}
		}
	}
	// Et ceux où la notion de chemin ne s'applique pas
	if (is_array($sanspath)) {
		foreach ($sanspath as $sp) {
			defined('_DIR_SITE') ? $base = _DIR_SITE : $base = _DIR_RACINE;
			if (stripos($sp[0], 'http://') === 0 || stripos($sp[0], 'https://')) {// On peut passer une url
				include_spip('inc/distant');
				$url = str_replace('&amp;', '&', $sp[0]);
				if ($sp[1]) {
					$chemin_fichier_recup = zippeur_chemin_dossier_local() . $dossier . '/' . $sp[1];
					zippeur_creer_arbo($dossier . '/' . $sp[1], 'oui');
					copie_locale($url, 'force', $chemin_fichier_recup);
				}
			}
			else {// pas url ?
				if (stripos($sp[0], $base) === false) {//vérifier que la personne n'a pas passé le chemin complet avant de modifier $sp[0]
					$sp[0] = $base . $sp[0];
				}
				$p = $sp[0];
				if ($sp[1] == '') {			// si le 2 argument est vide, alors pas de souci, on prend le chemin tel quel
					$sp[1] = $sp[0];
				}
				zippeur_copier_fichier($p, $dossier . '/' . $sp[1], false);
			}
		}
	}
	return zippeur([$chemin], $date, $cmd, $dossier, zippeur_chemin_dossier_local() . $dossier, $delai, $extension);
}

function zippeur($fichiers, $date = '', $cmd = 'defaut', $nom = 'defaut', $plat = 'oui', $delai = '0', $extension = 'zip') {
	if ($date == '') {
		$date = date('Y-m-d H:i:s', time());
	}
	$delai = valeur_numerique($delai);
	$nom == '' ? $nom = md5(serialize($fichiers)) : $nom = $nom;

	$chemin = zippeur_chemin_dossier_local() . 'cache-zip/' . $nom . '.' . $extension;
	include_spip('inc/flock');
	$enbase = sql_fetsel('id_zip,fichiers,date_modif', 'spip_zippeur', "`nom`='$nom' and `extension`='$extension'");
	/* On vérifie si le zip existe*/
	if (
		count(preg_files($chemin)) === 0
		|| !$enbase['id_zip']
		|| $enbase['date_modif'] != $date
		|| count($fichiers) != $enbase['fichiers']
		|| (
			defined('_NO_CACHE')
			&& _NO_CACHE != 0
			&& !defined('_NO_CACHE_SAUF_ZIPPEUR')
			)
	) {
		if (zippeur_zipper($chemin, $fichiers, $cmd, $plat)) {
			spip_log("Zippage de $nom.$extension avec cmd=$cmd", 'zippeur');
			if ($enbase['id_zip'] ?? '') {
				sql_updateq('spip_zippeur', ['delai_suppression' => $delai,'date_modif' => $date,'date_zip' => date('Y-m-d H-i-s'),'fichiers' => count($fichiers)], 'id_zip=' . $enbase['id_zip']);
			} else {
				sql_insertq('spip_zippeur', ['delai_suppression' => $delai,'nom' => $nom,'extension' => $extension,'date_modif' => $date,'date_zip' => date('Y-m-d H-i-s'),'fichiers' => count($fichiers)]);
			}
		}
	}
	return $chemin;
}

/**
 * Function qui fait le zip final
 * @param string $chemin
 * @param array $fichiers liste des fichiers
 * @param string $cmd @deprecated permettait dans le passé de choisir le mode de zip
 * @param string $plat si 'oui' (squelette) ou True (Php) alors on aplatit
 * @return bool false si erreur
**/
function zippeur_zipper($chemin, $fichiers, $cmd = 'defaut', $plat = true) {
	// Normalisation sur $plat
	if ($plat === 'oui' || $plat === true) {
		$plat = true;
	} else {
		$plat = false;
	}

	// Sécurité
	$fichiers = array_filter($fichiers);

	$temps_un = explode(' ', microtime());

	sous_repertoire(zippeur_chemin_dossier_local(), 'cache-zip');
	supprimer_fichier($chemin);

	include_spip('inc/archives');
	$zip = new Spip\Archiver\SpipArchiver($chemin);

	if (test_espace_prive()) {
		$fichiers = array_map(function ($f) {
return "../$f";
		}, $fichiers);
	}

	if ($plat) {
		$fichiers = array_flip($fichiers);
		foreach ($fichiers as $source => &$destination) {
			$destination = basename($source);
		}
	}

	$zip->emballer($fichiers);

	// Vérifications et retours finaux
	$erreur = $zip->erreur();
	if ($erreur) {
		$message_erreur = $zip->informer();
		spip_log($chemin . ' : ' . $message_erreur, 'zippeur_erreur' . _LOG_ERREUR);
	}

	$nb_fichiers_theoriques = count($fichiers);
	$zip_info = $zip->informer();
	$nb_fichiers_reels = count($zip_info['fichiers']);


	if ($nb_fichiers_theoriques != $nb_fichiers_reels) {
		spip_log("$chemin : $nb_fichiers_reels fichiers présents mais $nb_fichiers_theoriques prévus", 'zippeur_erreur' . _LOG_ERREUR);
		return false;
	} else {
		$temps_deux = explode(' ', microtime());
		spip_log('zipper en ' . ($temps_deux[1] - $temps_un[1]) . 'sec', 'zippeur');
		return true;
	}
}
