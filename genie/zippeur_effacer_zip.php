<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
function genie_zippeur_effacer_zip_dist($t) {
	// recherche dans la bdd
	define(_ZIPPEUR_MAX_EFFACER_ZIP, 1);
	$info = sql_select('id_zip,nom,extension', 'spip_zippeur', 'date_zip +INTERVAL delai_suppression SECOND < NOW() AND delai_suppression > 0', '', '', '0,' . _ZIPPEUR_MAX_EFFACER_ZIP);
	while ($ligne = sql_fetch($info)) {
		defined('_DIR_SITE') ? $chemin = _DIR_SITE . _NOM_TEMPORAIRES_ACCESSIBLES . 'cache-zip/' . $ligne['nom'] . '.' . $ligne['extension'] : $chemin = _DIR_RACINE . _NOM_TEMPORAIRES_ACCESSIBLES . 'cache-zip/' . $ligne['nom'] . '.' . $ligne['extension'];
		effacer_repertoire(zippeur_chemin_dossier_local() . $ligne['nom']);
		if (supprimer_fichier($chemin) || !file_exists($chemin)) {
			spip_log('Suppression de ' . $chemin, 'zippeur');
			sql_delete('spip_zippeur', 'id_zip=' . $ligne['id_zip']);
		}
	}
	return 0;
}
