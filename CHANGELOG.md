# Changelog

## 8.1.1 - 2024-12-20

### Fixed

- #4 Erreur dans le cron d'effacement des anciens zip

## 8.0.1 - 2023-11-16

### Fixed

- Charger systématiquement les classes de l'archiviste
- Le lien vers le zip doit avoir un attribut `type='application/zip'` pas `image/svg+xml`

## 8.0.0 - 2023-06-14
### Changed

- Retour du zippage à plat par défaut, meilleur perf pour le zippage à plat

### Removed

- Compatibilité spip < 4.2.3


## 7.0.1 - 2023-05-29

###  Fixed

- #3 Fix erreur lors du génie de nettoyage

## 7.0.0 - 2023-03-18

### Added

- Compatibilité SPIP 4.2

### Changed

- Par défaut les fichiers ne sont plus aplatis dans le zip, car plus consommateur de ressources

### Removed

- Plus qu'une seule méthode pour compresser les fichiers, celles fournies par plugins-dist/archiviste, le paramètre cmd des fonctions `zippeur_xx()` reste fournie pour compatibilité ascendante
- Compatibilité 4.0

- Appel d'autorisation incorrect dans l'action d'export en tableur

